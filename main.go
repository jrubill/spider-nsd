package main

import (
	_ "fmt"
	"net"
	"os"
	"strings"
)

/* 
	Client prefix            => Clnt:
	Primary Server prefix    => Psrv:
	Secondary Server prefix  => Ssrv:
	Server prefix for client => Serv:
 */

 var PREFIX_LENGTH = 5


type SpiderServer struct {
	Servers    map[string]string
	Clients    map[string]string
	IsPrimary  bool
}

func clientDatastream(conn net.Conn) {
	// do your actual programming logic here
	for {

	}
}

func (s *SpiderServer) handleClient(conn net.Conn, data string) {
	// accept client, add to registry
	addr := conn.RemoteAddr().String()
	
	// check if we've already seen the client before
	if _, ok := s.Clients[addr]; ok {
		clientDatastream(conn)
	} else {
		s.Clients[addr] = addr
	}
}

func (s *SpiderServer) handlePrimary(conn net.Conn, data string) {
	// ask for clients
	// be upgraded

}

func (s *SpiderServer) handleSecondary(conn net.Conn, data string) {
	// promote server
	// register server
	addr := conn.RemoteAddr().String()
	
	// check if we've seen this server before
	if _, ok := s.Clients[addr]; ok {

	} else {
		s.Clients[addr] = addr
	}
}

func (s *SpiderServer) handleConn(conn net.Conn) {
	buff := make([]byte, 1024)
	_, err := conn.Read(buff)
	if err != nil {
		panic(err)
	}
	resp := string(buff)
	payload := resp[PREFIX_LENGTH:]
	
	if strings.Contains(resp, "Clnt:") {
		s.handleClient(conn, payload)
	
	} else if strings.Contains(resp, "Psrv:") {
		s.handlePrimary(conn, payload)
	
	} else if strings.Contains(resp, "Ssrv:") {
		s.handleSecondary(conn, payload)
	
	} else {
		conn.Write([]byte("Invalid format. Closing connection"))
		conn.Close()
	}
	

	// conn.Write([]byte("Message Received."))
	conn.Close()
}

func server() {
	server, err := net.Listen("tcp", ":4050")
	spider := &SpiderServer{}
	if err != nil {
		os.Exit(0)
	}
	defer server.Close()
	for {
		conn, err := server.Accept()
		if err != nil {
			os.Exit(0)
		}
		go spider.handleConn(conn)
	}
}

func main() {
	server()
}