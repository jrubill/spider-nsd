# Spider NSD

Investigating a distributed system for network service discovery programs.


### Premise

- One individual opens up a server on the network and publishes a service. (primary server)
- Another individual opens up a server but sees the servie has already been published. (secondary server)
- This individual requests to be a secondary node of the first.
- Clients will be routed through the central node, and will be given the address of open servers.

### Subject Areas
- Distributed Systems
- Load Balancing
- Networking

### Potential Problems
- Primary server goes down.
- Promoting secondary servers to primary server.
- Maintaining record of connected secondary servers (new servers, deleted servers).
- Handle accidental two primary servers

## Message Formats
### Clients
Messages Sent
- Add:   `clnt: add`
- Ready: `clnt: ready`

Messages Received
- ConfirmAdded: `serv: confirmed`
- Redirect: `serv: redirect ipaddr:port`

### Primary Server
Messages Sent
- Client Redirect: `serv: redirect ipaddr:port`
- Confirm Server Add: `psrv: confirmed`
- Promote Secondary: `psrv: promote`

Messages Received
- ServerAdd: `ssrv: add`
- ServerReady: `ssrv: ready`
- ServerClose: `ssrv: close`

### Secondary Server
Messages Sent
- Ready: `ssrv: ready`

Messages Received

### Generic Server
Messages Sent
- RequestServerType: `serv: type`

Messages Received